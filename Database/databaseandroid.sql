-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2020 at 07:47 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databaseandroid`
--

-- --------------------------------------------------------

--
-- Table structure for table `daikin`
--

CREATE TABLE `daikin` (
  `Ma` varchar(10) NOT NULL,
  `Ten` varchar(255) NOT NULL,
  `Hinh` text NOT NULL,
  `ThoiGian` varchar(255) NOT NULL,
  `ThongTin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `daikin`
--

INSERT INTO `daikin` (`Ma`, `Ten`, `Hinh`, `ThoiGian`, `ThongTin`) VALUES
('001', '5 cách dùng điều hòa không khí tiết kiệm', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQifpcICwo8spErg-35itSqy1MrIyBPhIFqeQ&usqp=CAU', '09:12 -15 thg 02, 2020', 'https://shop.daikin.com.vn/dich-vu'),
('002', 'Review Trải nghiệm dịch vụ bảo trì', 'https://dienlanhs2.com/wp-content/uploads/2018/10/daikin-plasma.jpg', '12:43 -15 thg 02, 2020', 'https://shop.daikin.com.vn/danh-muc/may-loc-khong-khi?pageNum=1'),
('003', 'Trải Nghiệm dịch vụ từ E-shop của Daikin', 'https://i.pinimg.com/236x/cb/c8/f3/cbc8f39a20b0899b40f9c4bf1565796a--th%C3%A1i-lan-hoa.jpg', '22:43 -05 thg 09, 2020', 'https://shop.daikin.com.vn/danh-muc/sky-air?pageNum=1');

-- --------------------------------------------------------

--
-- Table structure for table `dakin`
--

CREATE TABLE `dakin` (
  `Hinh` varchar(255) NOT NULL,
  `Ten` varchar(255) NOT NULL,
  `ThoiGian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dakin`
--

INSERT INTO `dakin` (`Hinh`, `Ten`, `ThoiGian`) VALUES
('https://tse3.mm.bing.net/th?id=OIP.Z1c9a9Yrpu_h6RXq4h-n8gHaFj&pid=Api&P=0&w=212&h=160', 'Trải nghiệm dịch vụ bảo trì của Daikin', '10:19  -07 thg 11, 2020'),
('https://www.grassiasrl.com/wp-content/uploads/2018/02/Climatizzatore-Daikin-Sensira-18000-btu.jpg', 'Mua máy lạnh từ E-Shop của DaiKin', '10:19  -06 thg 11, 2020');

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

CREATE TABLE `donhang` (
  `madh` int(11) NOT NULL,
  `soluongsp` int(100) NOT NULL,
  `tongtien` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`madh`, `soluongsp`, `tongtien`) VALUES
(1, 0, 0),
(2, 2, 2),
(3, 2, 2),
(4, 2, 2),
(5, 2, 1382000),
(6, 1, 2900000),
(7, 1, 1900000);

-- --------------------------------------------------------

--
-- Table structure for table `phukien`
--

CREATE TABLE `phukien` (
  `masp` int(11) NOT NULL,
  `tensp` varchar(200) NOT NULL,
  `giasp` int(15) NOT NULL,
  `motasp` varchar(10000) NOT NULL,
  `hinhanh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phukien`
--

INSERT INTO `phukien` (`masp`, `tensp`, `giasp`, `motasp`, `hinhanh`) VALUES
(1, 'Dao cắt ống đồng cỡ lớn', 691000, 'ala', 'https://emin.vn/web/image/product.template/32085/wm_image/378x378/tascotb32n-dao-cat-ong-dong-co-lon-tasco-tb32n-32085'),
(2, 'Cờ lê lực Tone TSP', 1043000, 'lani', 'https://i.ytimg.com/vi/XWXkAjDBoy0/maxresdefault.jpg'),
(3, 'Kèm cắt ống', 100900, 'Sản xuất từ Việt Nam cơ sở lớn nhất hiện tại.', 'https://khaiphat.vn/upload/sanpham/093658.jpg'),
(4, 'Máy lọc nước', 2900000, 'Công nghệ Nano bạc diệt khuẩn ưu việt.\r\n02 vòi đa chức năng (01 vòi lấy nước RO, 01 vòi lấy nước NanoS)\r\n09 Cấp lọc mạnh mẽ (3 Lõi lọc thô, Màng RO 100GPD thay nhanh Mỹ, 2 Lõi T33-GAC; Lõi Far Infrared; Lõi Mineral; Lõi NanoSilver).', 'https://dienmaysakura.vn/media/product/331_kangaroo_kg100hk_chinh_dien.jpg'),
(5, 'Máy quạt hơi nước', 1900000, 'Quạt hơi nước Sanaky SNK-7200A với công suất làm mát cho phòng với diện tích 50-60m2. Máy làm mát hoạt động hiệu quả với nhiều nhu cầu khác nhau : gia đình, quán xá, nhà hàng, khách sạn, nhà xưởng…( không gian mở, bán mở) Đây là sản phẩm đang được yêu thích trong mùa hè với ưu điểm nổi trội và tiết kiệm tối đa.', 'https://sanakyvietnam.net/wp-content/uploads/quat-hoi-nuoc-sanaky-snk-7200a.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `Ma` varchar(10) NOT NULL,
  `Ten` varchar(50) NOT NULL,
  `Gia` decimal(10,0) NOT NULL,
  `Hinh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`Ma`, `Ten`, `Gia`, `Hinh`) VALUES
('101', 'Dao cạo ba via Tasco Japan ', '679000', 'https://cf.shopee.vn/file/65243a10f7823e6e7ec57ee42b8db0ce');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `hoten` varchar(255) NOT NULL,
  `namsinh` int(11) NOT NULL,
  `diachi` varchar(255) NOT NULL,
  `hinh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `hoten`, `namsinh`, `diachi`, `hinh`) VALUES
(9, 't', 0, 'ê', '');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE `taikhoan` (
  `id` int(11) NOT NULL,
  `taikhoan` varchar(50) NOT NULL,
  `matkhau` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`id`, `taikhoan`, `matkhau`) VALUES
(1, 'admin', 'admin'),
(3, 'duy00', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daikin`
--
ALTER TABLE `daikin`
  ADD PRIMARY KEY (`Ten`);

--
-- Indexes for table `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`madh`);

--
-- Indexes for table `phukien`
--
ALTER TABLE `phukien`
  ADD PRIMARY KEY (`masp`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Ma`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donhang`
--
ALTER TABLE `donhang`
  MODIFY `madh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `phukien`
--
ALTER TABLE `phukien`
  MODIFY `masp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `taikhoan`
--
ALTER TABLE `taikhoan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
