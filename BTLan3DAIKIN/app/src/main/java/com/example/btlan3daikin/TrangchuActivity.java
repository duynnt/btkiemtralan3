package com.example.btlan3daikin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TrangchuActivity extends AppCompatActivity {
    GridView gridView;
    TextView tt;
    ArrayList<profile> profileArrayList;
    profileAdapter profileAdapter;
    BottomNavigationView bottomNavigationView;
    LinearLayout lnbaotri, lnsuachua, lneshop, lnzalo, lndichvu, lnphukien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trangchu);
        anhxa();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(TrangchuActivity.this, gridview1.class);
                intent.putExtra("vitri",profileArrayList.get(i).getThongtin().toString());
//                intent.putExtra("tennhac",arrayList.get(i).getFile());
                startActivity(intent);
            }
        });
        lnbaotri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrangchuActivity.this,BaotriActivity.class));
            }
        });
        lnsuachua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrangchuActivity.this,Suachua.class));
            }
        });
        lneshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrangchuActivity.this,Eshop.class));
            }
        });
        lnzalo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callme = new Intent(Intent.ACTION_CALL);
                callme.setData(Uri.parse("tel:0368604577"));
                startActivity(callme);
            }
        });
        lndichvu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrangchuActivity.this,tab.class));
            }
        });
        lnphukien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrangchuActivity.this,phukien.class));
            }
        });
        readJson("http://192.168.42.28/androidWebsevices/daikin.php");
        profileAdapter = new profileAdapter(this, R.layout.dong_gridviewtc, profileArrayList);
        gridView.setAdapter(profileAdapter);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = null;
                switch (item.getItemId()){
                    case R.id.home:
                        fragment = new Fragment();
                        break;
                    case R.id.order:
                        fragment = new OrderPage();
                        break;
                    case R.id.machine:
                        fragment = new thietbipage();
                        break;
                    case R.id.message:
                        fragment = new ThongBaopage();
                        break;
                    case R.id.user:
                        fragment = new taikhoanpage();
                        break;
                }
                fragmentTransaction.replace(R.id.tests,fragment);
                fragmentTransaction.commit();
                return true;
            }
        });
    }

    public void readJson(String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                profileArrayList.clear();
                for (int i =0 ; i < response.length(); i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        profileArrayList.add(new profile(
                                object.getString("Ma"),
                                object.getString("Ten"),
                                object.getString("Hinh"),
                                object.getString("ThoiGian"),
                                object.getString("ThongTin")
                        ));
                    } catch (JSONException e) {
                        Toast.makeText(TrangchuActivity.this, "Lo lay", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                profileAdapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TrangchuActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        tt.setText(error.toString());
                        Log.d("AAA","Lỗi\n"+ error.toString());

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
    public void anhxa(){
        lndichvu= (LinearLayout) findViewById(R.id.lndichvu);
        lnbaotri = (LinearLayout) findViewById(R.id.lnbaotri);
        lnsuachua = (LinearLayout) findViewById(R.id.lnsuachua);
        lneshop = (LinearLayout) findViewById(R.id.lneshop);
        lnzalo = (LinearLayout) findViewById(R.id.lnzalo);
        lnphukien = (LinearLayout) findViewById(R.id.lnphukien);
        tt = (TextView) findViewById(R.id.loi);
        gridView = (GridView) findViewById(R.id.grapviewtc);
        profileArrayList = new ArrayList<>();
        bottomNavigationView = findViewById(R.id.btnview);
        ImageSlider imageSlider = (ImageSlider) findViewById(R.id.imgslider);
        List<SlideModel> slideModels = new ArrayList<>();
        slideModels.add(new SlideModel("https://img.websosanh.vn/v10/users/keydes/images/edf8wsvh3pciq/dieu-hoa-daikin.jpg",""));
        slideModels.add(new SlideModel("https://cdn.tgdd.vn/Products/Images/2002/219663/Slider/vi-vn-thumbdaikin.jpg",""));
        slideModels.add(new SlideModel("https://cafefcdn.com/zoom/700_438/pr/2020/1593830534453-0-19-364-601-crop-1593830546811-63729457941763.jpg",""));
        slideModels.add(new SlideModel("https://st.quantrimang.com/photos/image/2019/08/12/ma-loi-dieu-hoa-may-lanh-daikin-al.jpg",""));
        slideModels.add(new SlideModel("https://dieuhoanoithat.vn/uploads/files/Bai%20Vi%E1%BA%BFt/Hu%E1%BB%9Bng%20d%E1%BA%ABn%20s%E1%BB%AD%20d%E1%BB%A5ng%20di%E1%BB%81u%20hoa%20daikin%20chi%20ti%E1%BA%BFt%20nh%E1%BA%A5t%202020/Di%E1%BB%81u%20khi%E1%BB%83n%20daikin%20co%20c%E1%BA%A5u%20t%E1%BA%A1o%20nnt.jpg",""));
        slideModels.add(new SlideModel("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSBBkTWd3ubhfCJsnxXdHDZH_FODLaYmAU0g&usqp=CAU",""));
        imageSlider.setImageList(slideModels,true);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_anim);
        Animation animationoyt = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_out);
        imageSlider.setAnimation(animation);

//        profileArrayList.add(new profile("dd","https://st.quantrimang.com/photos/image/2019/08/12/ma-loi-dieu-hoa-may-lanh-daikin-al.jpg","dd"));

    }
}