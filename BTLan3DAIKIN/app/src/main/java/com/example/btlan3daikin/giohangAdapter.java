package com.example.btlan3daikin;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class giohangAdapter extends BaseAdapter {
    private giohang context;
    private int layout;
    private ArrayList<giohangshop> giohangshopList;

    public giohangAdapter(giohang context, int layout, ArrayList<giohangshop> giohangshopList) {
        this.context = context;
        this.layout = layout;
        this.giohangshopList = giohangshopList;
    }

    @Override
    public int getCount() {
        return giohangshopList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class ViewHolder{
        TextView txtten, txtgia;
        ImageView imghinh, btntang, btngiam;
        Button btnsolg;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(layout,null);
            holder.txtten = (TextView) convertView.findViewById(R.id.txttensp);
            holder.txtgia = (TextView) convertView.findViewById(R.id.txtgiasp);
            holder.imghinh = (ImageView) convertView.findViewById(R.id.idimg);
            holder.btntang = (ImageView) convertView.findViewById(R.id.btntang);
            holder.btngiam = (ImageView) convertView.findViewById(R.id.btngiam);
            holder.btnsolg = (Button) convertView.findViewById(R.id.so);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        giohangshop pr = giohangshopList.get(position);
        holder.txtten.setText(pr.getTen());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtgia.setText(decimalFormat.format(pr.getGia()));
        Picasso.get().load(pr.getHinh())
                .into(holder.imghinh);
        holder.btnsolg.setText(pr.getSoluong()+"");
        int slg = Integer.parseInt(holder.btnsolg.getText().toString());
        if (slg >= 10){
            holder.btntang.setVisibility(View.INVISIBLE);
            holder.btngiam.setVisibility(View.VISIBLE);
        }else if (slg <= 1){
            holder.btngiam.setVisibility(View.INVISIBLE);
        }else if (slg >=1){
            holder.btntang.setVisibility(View.VISIBLE);
            holder.btngiam.setVisibility(View.VISIBLE);
        }
        holder.btntang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int slnew = Integer.parseInt(holder.btnsolg.getText().toString()) +1 ;
                int slnow = phukien.giohangshops.get(position).getSoluong();
                int gianow = phukien.giohangshops.get(position).getGia();
                phukien.giohangshops.get(position).setSoluong(slnew);
                int gianew = (gianow * slnew) /slnow;
                phukien.giohangshops.get(position).setGia(gianew);
                DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                holder.txtgia.setText(decimalFormat.format(gianew)+ "Đ");
                giohang.tinhtinh();
                if (slnew >9){
                    holder.btntang.setVisibility(View.INVISIBLE);
                    holder.btngiam.setVisibility(View.VISIBLE);
                    holder.btnsolg.setText(String.valueOf(slnew));
                }else {

                        holder.btntang.setVisibility(View.VISIBLE);
                        holder.btngiam.setVisibility(View.VISIBLE);
                        holder.btnsolg.setText(String.valueOf(slnew));
                    
                }
            }
        });
        holder.btngiam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int slnew = Integer.parseInt(holder.btnsolg.getText().toString()) -1 ;
                int slnow = phukien.giohangshops.get(position).getSoluong();
                int gianow = phukien.giohangshops.get(position).getGia();
                phukien.giohangshops.get(position).setSoluong(slnew);
                int gianew = (gianow * slnew) /slnow;
                phukien.giohangshops.get(position).setGia(gianew);
                DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                holder.txtgia.setText(decimalFormat.format(gianew)+ "Đ");
                giohang.tinhtinh();
                if (slnew < 2){
                    holder.btngiam.setVisibility(View.INVISIBLE);
                    holder.btntang.setVisibility(View.VISIBLE);
                    holder.btnsolg.setText(String.valueOf(slnew));
                }else {
                    holder.btntang.setVisibility(View.VISIBLE);
                    holder.btngiam.setVisibility(View.VISIBLE);
                    holder.btnsolg.setText(String.valueOf(slnew));
                }
            }
        });
        return convertView;
    }
}
