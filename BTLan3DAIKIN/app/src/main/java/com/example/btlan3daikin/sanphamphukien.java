package com.example.btlan3daikin;

import java.io.Serializable;

public class sanphamphukien implements Serializable {
    private int id;
    private String tensp;
    private int gia;
    private String mota;
    private String hinhanh;

    public sanphamphukien(int id, String tensp, int gia, String mota, String hinhanh) {
        this.id = id;
        this.tensp = tensp;
        this.gia = gia;
        this.mota = mota;
        this.hinhanh = hinhanh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTensp() {
        return tensp;
    }

    public void setTensp(String tensp) {
        this.tensp = tensp;
    }

    public int getGia() {
        return gia;
    }

    public void setGia(int gia) {
        this.gia = gia;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }
}
