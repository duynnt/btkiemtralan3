package com.example.btlan3daikin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.VoiceInteractor;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    Button btnlogin, btndk;
    ArrayList<taikhoan> taikhoans;
    EditText edittk, editmk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        btndk = (Button) findViewById(R.id.dk);
        edittk = (EditText) findViewById(R.id.editsdt);
        editmk = (EditText) findViewById(R.id.editmk);
        taikhoans = new ArrayList<>();
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0 ; i< taikhoans.size(); i++){
                    if (edittk.getText().toString().trim().equals(taikhoans.get(i).getTaikhoan()) && editmk.getText().toString().trim().equals(taikhoans.get(i).getMatkhau())){
                        Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LoginActivity.this,TrangchuActivity.class));
                        break;
                    }else {
                        Toast.makeText(LoginActivity.this, "Tài khoản hoặc mật khẩu sai", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }
        });
        btndk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,dangki.class));
            }
        });
        getdata();

    }
    private void getdata(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, "http://192.168.42.28/androidWebsevices/slecttaikhoan.php", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i=0 ; i < response.length() ; i++){
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                taikhoans.add(new taikhoan(
                                   jsonObject.getInt("Id"),
                                        jsonObject.getString("TaiKhoan"),
                                        jsonObject.getString("MatKhau")
                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

}