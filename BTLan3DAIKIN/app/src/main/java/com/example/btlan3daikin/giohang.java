package com.example.btlan3daikin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.VoiceInteractor;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class giohang extends AppCompatActivity {
    Toolbar toolbar;
    ListView lv;
    static TextView txttongtien;
    Button btnthanhtoan;
    giohangAdapter giohangAdapter;
    String url = "http://192.168.42.28/androidWebsevices/insertgiohang.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giohang);
        anhxa();

        toolbar.setTitle("Giỏ Hàng");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),phukien.class));
            }
        });
        tinhtinh();
        btnthanhtoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("1")){
                            phukien.giohangshops.clear();
                            Toast.makeText(giohang.this, "Đặt hàng thành công", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(giohang.this,phukien.class));
                        }else {
                            Toast.makeText(giohang.this, "Vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        JSONArray jsonArray = new JSONArray();
                        int a=0, b=0;
                        for (int i = 0 ; i<phukien.giohangshops.size(); i++){
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("soluong",phukien.giohangshops.get(i).getSoluong());
                                jsonObject.put("tongtien",phukien.giohangshops.get(i).getGia());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            jsonArray.put(jsonObject);
                        }
                        HashMap<String,String> hashMap = new HashMap<>();
                        hashMap.put("json",jsonArray.toString());
                        return hashMap;
                    }
                };
                requestQueue.add(stringRequest);
            }
        });
    }

    public static void tinhtinh() {
        long tongtien = 0;
        for (int i =0 ;i<phukien.giohangshops.size();i++){
            tongtien += phukien.giohangshops.get(i).getGia();
        }
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        txttongtien.setText(decimalFormat.format(tongtien));
    }
    public void xacnhanxoa(int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(giohang.this);
        builder.setTitle("Xác nhận xóa sản phẩm");
        builder.setMessage("Bạn có chắc chắn xóa sản phẩm này");
        builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                phukien.giohangshops.remove(position);
                giohangAdapter.notifyDataSetChanged();
                tinhtinh();
            }
        });
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                giohangAdapter.notifyDataSetChanged();
                tinhtinh();
            }
        });
    }
    private void anhxa() {
        toolbar = (Toolbar) findViewById(R.id.tolbar);
        lv = (ListView) findViewById(R.id.lvgh);
        txttongtien = (TextView) findViewById(R.id.tongtien);
        btnthanhtoan = (Button) findViewById(R.id.thanhtoan);
        giohangAdapter = new giohangAdapter(this,R.layout.donggiohang,phukien.giohangshops);
        lv.setAdapter(giohangAdapter);

    }
}