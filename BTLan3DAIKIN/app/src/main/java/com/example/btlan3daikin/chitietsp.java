package com.example.btlan3daikin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class chitietsp extends AppCompatActivity {
    ImageView imgback, imgsp;
    TextView txtten, txtgia, txtmota;
    Button btnmua;
    int masp;
    String ten;
    String hinh;
    String mota;
    int gia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chitietsp);
        anhxa();
        getinformation();
        btnmua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phukien.giohangshops.size() > 0 ){
                    boolean ex = false;
                    for (int i=0 ; i<phukien.giohangshops.size(); i++){
                        if (phukien.giohangshops.get(i).getId() == masp){
                            phukien.giohangshops.get(i).setSoluong(phukien.giohangshops.get(i).getSoluong()+ 1);
                            phukien.giohangshops.get(i).setGia(gia*phukien.giohangshops.get(i).getSoluong());
                            ex = true;
                        }
                    }
                    if (ex == false){
                        int soluong = 1;
                        long giamoi = soluong *gia;
                        phukien.giohangshops.add(new giohangshop(masp,ten,gia,hinh,soluong));
                    }

                }else {
                    int soluong = 1;
                    long giamoi = soluong *gia;
                    phukien.giohangshops.add(new giohangshop(masp,ten,gia,hinh,soluong));

                }
                Intent intent = new Intent(getApplicationContext(),giohang.class);
                startActivity(intent);
            }
        });
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getinformation() {
        sanphamphukien sanphamphukien = (com.example.btlan3daikin.sanphamphukien) getIntent().getSerializableExtra("thongtinsanpham");
        masp = sanphamphukien.getId();
        ten = sanphamphukien.getTensp();
        mota = sanphamphukien.getMota();
        hinh = sanphamphukien.getHinhanh();
        gia = sanphamphukien.getGia();
        txtten.setText(ten);
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        txtgia.setText("Giá:" + decimalFormat.format(gia)+"Đ");
        txtmota.setText(mota);
        Picasso.get().load(hinh).into(imgsp);
    }

    private void anhxa() {
        imgback = (ImageView) findViewById(R.id.back);
        imgsp = (ImageView) findViewById(R.id.imgbanner);
        txtten = (TextView) findViewById(R.id.txtten);
        txtgia = (TextView) findViewById(R.id.txtgia);
        txtmota = (TextView) findViewById(R.id.txtmota);
        btnmua = (Button) findViewById(R.id.btnmua);
    }
}