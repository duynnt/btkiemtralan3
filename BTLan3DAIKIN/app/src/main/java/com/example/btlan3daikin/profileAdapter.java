package com.example.btlan3daikin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class profileAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<profile> profileList;

    public profileAdapter(Context context, int layout, List<profile> profileList) {
        this.context = context;
        this.layout = layout;
        this.profileList = profileList;
    }

    @Override
    public int getCount() {
        return profileList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class ViewHolder{
        TextView txtten, txttime;
        ImageView imghinh;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(layout,null);
            holder.txtten = (TextView) convertView.findViewById(R.id.txtten);
            holder.txttime = (TextView) convertView.findViewById(R.id.txttime);
            holder.imghinh = (ImageView) convertView.findViewById(R.id.imagesview);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        profile pr = profileList.get(position);
        holder.txtten.setText(pr.getTen());
        holder.txttime.setText(pr.getTime());
        Picasso.get().load(pr.getHinh())
                .into(holder.imghinh);
        return convertView;
    }
}
