package com.example.btlan3daikin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class dangki extends AppCompatActivity {
    Button btndk, btndn;
    EditText edtuser, editpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangki);
        btndk = (Button) findViewById(R.id.btndangkitk);
        btndn = (Button) findViewById(R.id.btndangnhap);
        edtuser = (EditText) findViewById(R.id.editusername);
        editpass = (EditText) findViewById(R.id.editpasswork);
        btndn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(dangki.this,LoginActivity.class));
            }
        });
        btndk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtuser.getText().toString().trim() != "" && editpass.getText().toString().trim() != ""){
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://192.168.42.28/androidWebsevices/inserttaikhoan.php", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.equals("TC")){
                                Toast.makeText(dangki.this, "Đăng kí thành công", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(dangki.this, "Tài khoản đã tồn tại", Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(dangki.this, "sever", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> param = new HashMap<>();
                            param.put("taikhoan",edtuser.getText().toString().trim());
                            param.put("matkhau",editpass.getText().toString().trim());
                            return param;
                        }
                    };
                    requestQueue.add(stringRequest);
                }else {
                    Toast.makeText(dangki.this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}