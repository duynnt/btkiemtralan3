package com.example.btlan3daikin;

import java.io.Serializable;

public class profile implements Serializable {
    private String ma;
    private String ten;
    private String hinh;
    private String time;
    private String thongtin;

    public profile(String ma, String ten, String hinh, String time, String thongtin) {
        this.ma = ma;
        this.ten = ten;
        this.hinh = hinh;
        this.time = time;
        this.thongtin = thongtin;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getThongtin() {
        return thongtin;
    }

    public void setThongtin(String thongtin) {
        this.thongtin = thongtin;
    }
}
