package com.example.btlan3daikin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class phukien extends AppCompatActivity {
    GridView gridView;
    ArrayList<sanphamphukien> profileArrayList;
    sanphamphukienAdapter sanphamphukienAdapter;
    ImageView imageout, imggiohang;
    public static ArrayList<giohangshop> giohangshops;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phukien);
        anhxa();
        imageout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(phukien.this,TrangchuActivity.class));
            }
        });
        imggiohang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(phukien.this,giohang.class));
            }
        });
        gridView = (GridView) findViewById(R.id.grapviewsp);
        profileArrayList = new ArrayList<>();
        sanphamphukienAdapter = new sanphamphukienAdapter(profileArrayList,this, R.layout.dong_gridviewtc);
        gridView.setAdapter(sanphamphukienAdapter);
        readJson("http://192.168.42.28/androidWebsevices/showproduct.php");
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), chitietsp.class);
                intent.putExtra("thongtinsanpham",profileArrayList.get(i));
//                intent.putExtra("tennhac",arrayList.get(i).getFile());
                startActivity(intent);
            }
        });
    }

    private void anhxa() {
        imggiohang = (ImageView) findViewById(R.id.idgiohang);
        imageout = (ImageView) findViewById(R.id.out);
        if (giohangshops !=null){

        }else {
            giohangshops = new ArrayList<>();
        }
    }

    public void readJson(String url){
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                profileArrayList.clear();
                for (int i =0 ; i < response.length(); i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        profileArrayList.add(new sanphamphukien(
                                object.getInt("Ma"),
                                object.getString("Ten"),
                                object.getInt("Gia"),
                                object.getString("MoTa"),
                                object.getString("Hinh")
                        ));
                    } catch (JSONException e) {
                        Toast.makeText(phukien.this, "Lo lay", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                sanphamphukienAdapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(phukien.this, error.toString(), Toast.LENGTH_SHORT).show();
//                        Log.d("AAA","Lỗi\n"+ error.toString());

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }
}