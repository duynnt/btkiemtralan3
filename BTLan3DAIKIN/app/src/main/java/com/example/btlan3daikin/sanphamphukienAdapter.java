package com.example.btlan3daikin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class sanphamphukienAdapter extends BaseAdapter {
    ArrayList<sanphamphukien> arrayList;
    Context context;
    private int layout;

    public sanphamphukienAdapter(ArrayList<sanphamphukien> arrayList, Context context, int layout) {
        this.arrayList = arrayList;
        this.context = context;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    public class ViewHolder{
        TextView txtten, txtgia;
        ImageView imghinh;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(layout,null);
            holder.txtten = (TextView) convertView.findViewById(R.id.txtten);
            holder.txtgia = (TextView) convertView.findViewById(R.id.txttime);
            holder.imghinh = (ImageView) convertView.findViewById(R.id.imagesview);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        sanphamphukien pr = arrayList.get(position);
        holder.txtten.setText(pr.getTensp());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
//        txtgia.setText("Giá:" + decimalFormat.format(gia)+"Đ");
        holder.txtgia.setText("Giá:" + decimalFormat.format(pr.getGia())+"Đ");
        Picasso.get().load(pr.getHinhanh())
                .into(holder.imghinh);
        return convertView;
    }
}
